#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <time.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>
#include <signal.h>


struct radio{
  char* ipv4_mul;
  char* ipv4_loc;
  char* id;
  char* port_multi;
  char* port_message;
  char** list_message;
  int pointeur_mess;
};

char* addr_ge;
char* port_ge;
pthread_mutex_t verrou = PTHREAD_MUTEX_INITIALIZER;
struct radio* diffuseur;
char* prochain_message[10];
int num_attente;
int affiche_mess;

char* get_line(){
  char* line = malloc(100), * linep = line;
  size_t lenmax = 100, len = lenmax;
  int c;
  if(line == NULL)
    return NULL;
  for(;;) {
    c = fgetc(stdin);
    if(c == EOF)
      break;
    if(--len == 0) {
      len = lenmax;
      char * linen = realloc(linep, lenmax *= 2);
      if(linen == NULL) {
	free(linep);
	return NULL;
      }
      line = linen + (line - linep);
      linep = linen;
    }
    if((*line++ = c) == '\n')
      break;
  }
  *line = '\0';
  return linep;
}


char* get_time(){
  time_t rawtime;
  struct tm *timeinfo;
  time(&rawtime);
  timeinfo = localtime(&rawtime);
  return(asctime(timeinfo));
}

void strreplace(char s[], char chr, char repl_chr){
  int i=0;
  while(s[i]!='\0'){
    if(s[i]==chr){
      s[i]=repl_chr;
    }
    i++;
  }
  printf("%s",s);
}

void INThandler(int sig){
  char  c;
  int i;
  affiche_mess = 1;
  signal(sig, SIG_IGN);
  printf("%.24s : Demande de fermeture du diffuseur !\n\n", get_time());
  printf("Voulez vous vraiment quitter [Y/n] : ");
  c = getchar();
  if(c == 'y' || c == 'Y'){
    printf("\n\n%.24s : Fermeture du diffuseur !\n\n", get_time());
    free(diffuseur -> ipv4_mul);
    free(diffuseur -> ipv4_loc);
    free(diffuseur -> port_multi);
    free(diffuseur -> port_message);
    free(diffuseur -> id);
    for(i = 0; i < 9999; i++){
      if(diffuseur -> list_message[i] != NULL){
	free(diffuseur -> list_message[i]); 
      }
    }
    free(diffuseur -> list_message);
    free(diffuseur);  
    exit(0);
  }
  else signal(SIGINT, INThandler);
  affiche_mess = 0;
  getchar(); // Get new line character
}


void* start_multicast(){
  int sock;
  int r = 0, stop = 0, message_cmp = 0, from_data = 0;
  char* message = NULL;
  char* pointeur = NULL;
  printf("%.24s : Lancement du thread de multicast, intervalle entre les diffusions 3 secondes.\n\n", get_time());
  /*Mise en place de la socket*/
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if(sock < 0){
    perror("socket");
    exit(1);
  }
  struct addrinfo* first_info;  
  r = getaddrinfo(diffuseur -> ipv4_mul, diffuseur -> port_multi, NULL, &first_info);
  if(r < 0){
    perror("getaddrinfo");
    exit(1);
  }
  if(first_info == NULL){
    perror("first_info");
    exit(1);
  }
  struct sockaddr* saddr = first_info -> ai_addr;
  socklen_t saddrlen = (socklen_t)sizeof(struct sockaddr_in);
  char buffer[159];
  while(!stop){
    //message = get_next_message(); TODO ecrire une fonction qui choisira le prochain message à envoyer(d'un client ou alors d'une base de contenant une liste de message)
    pthread_mutex_lock(&verrou);
    if(num_attente == 0){
      message = "Message de la base";
      from_data = 1;
    }
    else{
      message = prochain_message[--num_attente];
      strreplace(message, '#', ' ');
      from_data = 0;
    }
    snprintf(buffer, sizeof(buffer), "DIFF %i %.8s %s\r\n", message_cmp, diffuseur -> id, message);
    if(from_data == 0) free(message);
    pthread_mutex_unlock(&verrou);
    r = sendto(sock, buffer, strlen(buffer), 0, saddr, saddrlen);
    if(r < 0){
      perror("sendto");
      exit(1);
    }
    else{
      if(affiche_mess == 0){
	printf("%.24s : Message envoyé | %s\n", get_time(), buffer);
      }
      message_cmp = message_cmp % 9999;
      if(diffuseur -> list_message[message_cmp] == NULL){
	pointeur = malloc(sizeof(char) * 140);
      }
      else{
	pointeur = diffuseur -> list_message[message_cmp];
      }
      strcpy(pointeur, buffer+5);
      diffuseur -> list_message[message_cmp++] = pointeur;
      diffuseur -> pointeur_mess = message_cmp;
    }
    sleep(3);
  }
  printf("%.24s : Fermeture du diffuseur\n", get_time());
  return NULL;
}

void* compute_input(void* args){
  int socket = *(int*)args;
  char input[256];
  char buff_send[256];
  int i = 0;
  int nb_message;
  int recu = recv(socket, input, sizeof(input), 0);
  input[recu] = '\0';
  printf("%.24s : Lancement du thread de communication avec client\n\n", get_time());
  if(strcmp(input, "MESS\r\n")){
    printf("%.24s : Message recu d'un utilisateur pour la diffusion\n\n", get_time());
    if(sizeof(input + 14) > 140){
      printf("%.24s : Taille du message trop grande, demande refusée\n\n", get_time());
    }
    else{
      pthread_mutex_lock(&verrou);
      send(socket, "ACKM\r\n", 6, 0);
      printf("%.24s : Ajout du message à la liste d'attente\n\n", get_time());
      if(num_attente == 10){
	printf("%.24s : Message refusé, trop de message en attente\n\n", get_time());
      }
      if(num_attente == 0){
	prochain_message[num_attente] = malloc(sizeof(char) * 140);
	strcpy(prochain_message[num_attente++], input + 5); 
      }
      else{
	for(i = num_attente - 1; i >= 0; i--){
	  prochain_message[i + 1] = prochain_message[i];
	}
	prochain_message[0] = malloc(sizeof(char) * 140);
	strcpy(prochain_message[0], input + 5);
	num_attente++;
      }
      pthread_mutex_unlock(&verrou);
    }
  }
  if(input[0] == 'L' && input[1] == 'A' && input[2] == 'S' && input[3] == 'T'){//TODO meme chose que pour mess
    pthread_mutex_lock(&verrou);
    nb_message = diffuseur -> pointeur_mess;
    int nb_envoi = atoi(input + 5);
    printf("%.24s : Demande des %i derniers message\n\n", get_time(), nb_envoi);
    if(nb_envoi > nb_message){
      nb_envoi = nb_message;
    }
    i = (nb_message) - 1;
    for(; i >= (nb_message) - nb_envoi; i--){
      snprintf(buff_send, sizeof(buff_send), "OLDM %s\r\n", diffuseur -> list_message[i]);
      send(socket, buff_send, sizeof(buff_send), 0);
    }
    send(socket, "ENDM\r\n", 6, 0);
    pthread_mutex_unlock(&verrou);
  }
  close(socket);
  return NULL;
}

int reconnect_gestio(){
  int* socket_gestio = malloc(sizeof(int));
  *socket_gestio = socket(PF_INET, SOCK_STREAM, 0);
  struct sockaddr_in* addrin_gestio;
  struct addrinfo* info_gestio;
  struct addrinfo hints;
  bzero(&hints, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  int r = getaddrinfo(addr_ge, port_ge, &hints, &info_gestio);
  if(r < 0){
    perror("getaddrinfo");
    exit(1);
  }
  if(info_gestio == NULL){
    printf("%.24s : Impossible de recuperer les informations sur le gestionaire\n\n", get_time());
    exit(1);
  }
  addrin_gestio = (struct sockaddr_in *)info_gestio -> ai_addr;
  char buff_send[128];
  char buff_rec[256];
  while(1){
    r = connect(*socket_gestio, (struct sockaddr *)addrin_gestio, (socklen_t)sizeof(struct sockaddr_in));
    if(r < 0){
      perror("connect");
      printf("\n%.24s : Tentative de reconnexion avec le gestionnaire échouée\n\n", get_time());
      sleep(5);
      continue;
    }
    snprintf(buff_send, sizeof(buff_send), "REGI %.8s %s %s %s %s\r\n", diffuseur -> id, diffuseur -> ipv4_mul,
	     diffuseur -> port_multi, diffuseur -> ipv4_loc, diffuseur -> port_message);
    send(*socket_gestio, buff_send, sizeof(buff_send), 0);
    printf("%.24s : Demande de connexion envoyée\n\n", get_time());
    int rec = recv(*socket_gestio, buff_rec, sizeof(buff_rec), 0);
    buff_rec[rec] = '\0';
    printf("%.24s : Message du gestionnaire : %s \n", get_time(), buff_rec);
    if(strcmp(buff_rec, "RENO\r\n") == 0){
      printf("%.24s : Le gestionnaire a refusé la connection.\n\n", get_time());
      close(*socket_gestio);
      continue;
    }
    if(!(strcmp(buff_rec, "REOK\r\n") == 0)){
      printf("%.24s : Le gestionnaire est fou, son message : %s\n\n", get_time(), buff_rec);
      continue;
    }
    printf("%.24s : Reconnexion avec le gestionnaire réussie\n\n", get_time());
    return *socket_gestio;
  }
}
void* comm_gestio(void* args){
  int socket = *(int*)args;
  char buff_rec[128] = {0};
  char buff_send[128] = {0};
  printf("%.24s : Le thread gestionnaire a démarré\n\n", get_time());
  while(1){
    memset(buff_rec, '\0', sizeof(buff_rec));
    memset(buff_send, '\0', sizeof(buff_send));
    int recu = recv(socket, buff_rec, sizeof(buff_rec), 0);
    buff_rec[recu] = '\0';
    //printf("Message du gestionnaire %s \n", buff_rec);
    if(recu <= 0){
      perror("recv");
      printf("\n%.24s : Le gestionnaire à été déconnecté!!\n\n", get_time());
      socket = reconnect_gestio();
    }
    buff_rec[recu] = '\0';
    if(strcmp(buff_rec, "RUOK\r\n") == 0){
      snprintf(buff_send, sizeof(buff_send), "IMOK\r\n");
      sleep(3);
      if(send(socket, buff_send, strlen(buff_rec), 0) < 0){
	perror("send");
      }
      if(affiche_mess == 0){
	printf("%.24s : BLIP\n\n", get_time());
      }
    }
    else{
      printf("%.24s : Message du gestionnaire %s \n\n", get_time(), buff_rec);
    }
  }
  close(socket);
}
  
void* start_routine(){
  int* socket_gestio = malloc(sizeof(int));
  *socket_gestio = socket(PF_INET, SOCK_STREAM, 0);
  struct sockaddr_in* addrin_gestio;
  struct addrinfo* info_gestio;
  struct addrinfo hints;
  bzero(&hints, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  int r = getaddrinfo(addr_ge, port_ge, &hints, &info_gestio);
  if(r < 0){
    perror("getaddrinfo");
    exit(1);
  }
  if(info_gestio == NULL){
    printf("%.24s : Impossible de recuperer les informations sur le gestionaire\n\n", get_time());
    exit(1);
  }
  addrin_gestio = (struct sockaddr_in *)info_gestio -> ai_addr;
  r = connect(*socket_gestio, (struct sockaddr *)addrin_gestio, (socklen_t)sizeof(struct sockaddr_in));
  if(r < 0){
    perror("connect");
    printf("%.24s : Connexion impossible avec le gestionnaire\n\n", get_time());
    exit(1);
  }
  char buff_send[128] = {0};
  char buff_rec[256] = {0};
  snprintf(buff_send, sizeof(buff_send), "REGI %.8s %s %s %s %s\r\n", diffuseur -> id, diffuseur -> ipv4_mul, 
	   diffuseur -> port_multi, diffuseur -> ipv4_loc, diffuseur -> port_message);
  printf("%s\n", buff_send);
  send(*socket_gestio, buff_send, strlen(buff_send), 0);
  printf("%.24s : Demande de connexion envoyée\n\n", get_time());
  int rec = recv(*socket_gestio, buff_rec, sizeof(buff_rec), 0);
  buff_rec[rec] = '\0';
  //printf("Message du gestionnaire : %s \n", buff_rec);
  if(strcmp(buff_rec, "RENO\r\n") == 0){
    printf("%.24s : Le gestionnaire a refusé la connexion\n", get_time());
    close(*socket_gestio);
    exit(0);
  }
  if(!(strcmp(buff_rec, "REOK\r\n") == 0)){
    printf("%.24s : Le gestionnaire est fou, son message : %s\n", get_time(), buff_rec);
    exit(1);
  }
  printf("%.24s : Le gestionnaire a accepté la connexion\n\n", get_time());
  pthread_t thread_gestio;
  pthread_create(&thread_gestio, NULL, comm_gestio, socket_gestio);
  int* socket_client = malloc(sizeof(int));
  *socket_client = socket(PF_INET, SOCK_STREAM, 0);
  struct sockaddr_in addrin_client;
  addrin_client.sin_family = AF_INET;
  addrin_client.sin_port = htons(atoi(diffuseur -> port_message));
  addrin_client.sin_addr.s_addr = htonl(INADDR_ANY);
  r = bind(*socket_client, (struct sockaddr *)&addrin_client, sizeof(struct sockaddr_in));
  if(r < 0){
    perror("bind");
    exit(1);
  }
  r = listen(*socket_client, 0);
  if(r < 0){
    perror("listen");
    exit(1);
  }
  struct sockaddr_in caller;
  socklen_t size = sizeof(caller);
  while(1){
    int* sock_multi_thread = (int*) malloc(sizeof(int));
    *sock_multi_thread = accept(*socket_client, (struct sockaddr *)&caller, &size);
    if(*sock_multi_thread >= 0){
      printf("%.24s : Demande de connexion, nouvelle thread\n", get_time());
      pthread_t th;
      pthread_create(&th, NULL, compute_input, sock_multi_thread);
    }
    else{
      perror("accept");
      exit(1);
    }
  }
  close(*socket_client);
  close(*socket_gestio);
  free(socket_client);
  free(socket_gestio);
  return NULL;
}

int main(int argc, char* argv[]){
  int i;
  int taille;
  char* ip_mul;
  char* ip_loc;
  char* nom;
  char* port_mu;
  char* port_me;
  printf("Entrez l'id du diffuseur : ");
  nom = get_line();
  taille = strlen(nom) - 1;
  if(taille > 8){
    printf("\nNom du diffuseur trop long !\n");
    exit(0);
  }
  printf("taille %i\n", taille);
  for(i = taille; i < 8; i++){
    nom[i] = '#';
  }
  if(argc > 1 && strcmp(argv[1], "-o") == 0){
    printf("\nEntrez l'ip de multidiffusion : ");
    ip_mul = get_line();
    ip_mul[strlen(ip_mul) - 1] = '\0';
    printf("\nEntrez le port de multidiffusion : ");
    port_mu = get_line();
    port_mu[strlen(port_mu) - 1] = '\0';
    printf("\nEntrez l'ip du diffuseur pour la communication TCP : ");
    ip_loc = get_line();
    ip_loc[strlen(ip_loc)- 1] = '\0';
    printf("\nEntrez le port du diffuseur pour la communication TCP : ");
    port_me = get_line();
    port_me[strlen(port_me)- 1] = '\0';
    printf("\nEntrez l'ip du gestionnaire : ");
    addr_ge = get_line();
    addr_ge[strlen(addr_ge)- 1] = '\0';
    printf("\nEntrez le port du gestionnaire : ");
    port_ge = get_line();
    port_ge[strlen(port_ge)- 1] = '\0';
  }
  else{
    ip_mul = malloc(sizeof(char) * 100);
    port_mu = malloc(sizeof(char) * 100);
    ip_loc = malloc(sizeof(char) * 100);
    port_me = malloc(sizeof(char) * 100);
    addr_ge = malloc(sizeof(char) * 100);
    port_ge = malloc(sizeof(char) * 100);
    strcpy(ip_mul, "225.111.111.111");
    strcpy(port_mu, "6666");
    strcpy(ip_loc, "172.128.173.171");
    strcpy(port_me, "6665");
    strcpy(addr_ge, "localhost");
    strcpy(port_ge, "9034");
  }
  signal(SIGINT, INThandler);
  diffuseur = malloc(sizeof(struct radio));
  diffuseur -> ipv4_mul = malloc(sizeof(char) * 16);
  diffuseur -> ipv4_loc = malloc(sizeof(char) * 16);
  diffuseur -> port_multi = malloc(sizeof(char) * 5);
  diffuseur -> port_message = malloc(sizeof(char) * 5);
  diffuseur -> id = malloc(sizeof(char) * 9);
  diffuseur -> list_message = malloc(sizeof(char**) * 9999);
  for(i = 0; i < 9999; i++){
    diffuseur -> list_message[i] = NULL; 
  }
  strcpy(diffuseur -> ipv4_mul, ip_mul);
  strcpy(diffuseur -> ipv4_loc, ip_loc);
  strcpy(diffuseur -> port_multi, port_mu);
  strcpy(diffuseur -> port_message, port_me);
  strcpy(diffuseur -> id, nom);
  free(ip_mul);
  free(ip_loc);
  free(port_mu);
  free(port_me);
  free(nom);
  affiche_mess = 0;
  pthread_t thread1;
  pthread_t thread2;
  pthread_create(&thread1, NULL, start_multicast, NULL);
  pthread_create(&thread2, NULL, start_routine, NULL);
  /*  connect_gestionnair(&rad); // thread 1
      start_multicast(&rad); // thread 2*/
  pthread_join(thread1, NULL);
  pthread_join(thread2, NULL);
  free(diffuseur -> ipv4_mul);
  free(diffuseur -> ipv4_loc);
  free(diffuseur -> port_multi);
  free(diffuseur -> port_message);
  free(diffuseur -> id);
  for(i = 0; i < 9999; i++){
    if(diffuseur -> list_message[i] != NULL){
      free(diffuseur -> list_message[i]); 
    }
  }
  free(diffuseur -> list_message);
  free(diffuseur);
  return 0;
}
