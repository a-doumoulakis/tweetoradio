import java.net.*;
import java.io.*;

class ecouteDiffuseur implements Runnable {
    public String info  ;
    public int port ; 
    public String adresse ; 
    public boolean Allumer  = true ; 
    public ecouteDiffuseur (String m ) {
	this.info = m ; 
    }

    public void run () {
	String [] decoup = info.split( " ") ; 
	System.out.println("ecoutediffuseur run " + decoup[0]) ; 
	try {
	     this.port = Integer.parseInt(decoup[2]);
	    this.adresse = decoup[1] ; 
	    MulticastSocket ms = new MulticastSocket(this.port ) ;
	    ms.joinGroup(InetAddress.getByName(this.adresse ) ) ; 
	    byte [] data = new byte [150] ;
	    DatagramPacket paquet = new DatagramPacket (data , data.length) ;
	    while(true) {
		ms.receive(paquet) ;
		String tmp = new String (paquet.getData() , 0 , paquet.getLength() ) ; 
		System.out.println(tmp) ; 
	    }
		
	    }
	catch (Exception e ) {
	    System.out.println("erreur dans run ecoute\n" + e ) ; 
	    e.printStackTrace() ; 
	}
	System.out.println("sortie" ) ; 
    }
}

class ecrireAuDiffuseur implements Runnable{
    public String message ;
    public int port ;
    public int etat ; 
    public String adresse;
    public String id ; 
    public boolean Allumer = true ; 
    public ecrireAuDiffuseur(String add , String p , String m , String i) {
	this.message  = m ;
	this.adresse = add ; 
	this.id = i ; 
	String [] tab = message.split(" ") ; 
	if (tab[0].equals("MESS") ) {
	    int tailleMessage = message.length() - tab[0].length() + 1;
		if ((tailleMessage >0 )&&(tailleMessage < 141) ) {
		    etat = 0 ;
		}
		else {
		    etat = 2 ; 
		}
	}
	else if ((tab[0].equals("LAST") )&&(tab.length ==2 ) ){
	    etat = 1 ; 
	}
	else {
	    etat = 2 ; 
	}
	try {
	    this.port = Integer.parseInt(p) ; 
	}
	catch(Exception e ) {
	    etat = 2 ;
	}
    }
    public String assemblage() {
	String []tab =this.message.split(" ");
	String fini = tab[0] + " " + this.id;
	for(int i = 1 ; i <tab.length ; i++ ) {
	    fini += " " + tab[i] ;
	}
	return fini ; 

    }
    public void run () {
	try {
	    System.out.println("add " + this.adresse + " p "+ this.port);
	    Socket s = new Socket (this.adresse , this.port) ; 
	    BufferedReader br = new BufferedReader (new InputStreamReader ( s.getInputStream() )) ;
	    PrintWriter pw = new PrintWriter(new OutputStreamWriter (s.getOutputStream())) ; 	 
	    if(etat == 0 ) {
		String m = assemblage() ; 
		pw.write(m) ; 
		pw.flush() ;
		String fin  = br.readLine() ;
		if (fin.equals("ACKM") ) {
		    System.out.println(fin) ; 
		}
		else {
		    Allumer = false ; 
		    System.out.println("erreur dans ecrire run 0 " );
		}
	    }
	    else if (etat == 1 ) {
		try { 
		    String [] testons = this.message.split(" ") ;  
		    Integer.parseInt(testons[2]);
		    boolean lire = true ; 
		    while (lire && Allumer) {
			String ligne = br.readLine() ; 
			System.out.println(ligne) ; 
			if(ligne.equals("ENDM")) {
			lire = false ; 
			}
		    }
		}
		catch(Exception e) {
		    System.out.println("erreur" ) ; 
		    Allumer = false  ; 
		}
	    }
	    else {
		Allumer = false  ; 
		System.out.println("message du mauvais format ") ; 
	    }
	
	}
	catch (Exception e) {
	    Allumer =false ; 
	    System.out.println(e) ;
	    e.printStackTrace() ; 
	}
    }

}
class Client{

    public static String[] afficheDiffuseur () {
	try {
	    Socket sock = new Socket("localhost" , 9034) ;
	    BufferedReader br = new BufferedReader (new InputStreamReader ( sock.getInputStream() )) ;
	    PrintWriter pw = new PrintWriter(new OutputStreamWriter (sock.getOutputStream())) ; 
	    pw.write("LIST\n");
	    pw.flush() ; 
	    String  mess = br.readLine() ; 
	    int nombredeMessage = 0 ;
	    try  {
		String nombre ; 
		if(mess.length() == 6) {
		    nombre = ""+mess.charAt(5) ; 
		    nombredeMessage =Integer.parseInt( nombre );
		}
		else if (mess.length() == 7 ) {
		    nombre ="" +  mess.charAt(5) + mess.charAt(6) ;
		    nombredeMessage =Integer.parseInt( nombre );
		}
		else {
		    System.out.println("erreur que je vien de rajouter ") ; 
		   
		}
		
	    }
	    catch(Exception e ) {
		System.out.println("Erreur dans la liste des diffuseur , message non comforme") ; 
	    }
	    String [] listeDiffuseur = null ;  
	    if(nombredeMessage > 0 ) {
		listeDiffuseur = new String[nombredeMessage] ;
		int pointeur =  0 ; 
		while (nombredeMessage !=0 ) {
		    String diffuseur = br.readLine() ; 
		    listeDiffuseur[pointeur] = diffuseur ; 
		    System.out.println(diffuseur ) ; 
		    nombredeMessage -- ; 
		    pointeur ++ ; 
		}
	    }
	    br.close() ;
	    pw.close() ;
	    sock.close() ; 
	    return listeDiffuseur ; 
	}
	catch (Exception e ) {
	    System.out.println("erreur dans afficheDiffuseur\n"+ e) ; 
	    e.printStackTrace() ; 
	}
	return null;
    }

    public static String enleverDiez(String idDiez ) {
	String retour = ""; 
	for(int i = 0 ; i <idDiez.length() ; i ++) {
	    if (idDiez.charAt(i) != '#') {
		retour += idDiez.charAt(i) ; 
	    }
	    else {
		return retour; 
	    }
	}
	return ""; 
    }
    
    public static String verif(String [] tab , String id ) {
	for (int i = 0 ; i < tab.length ; i++ ) {
	    String[] intermediaire = tab[i].split(" ") ; 
	    if (intermediaire.length == 6) {
		String idDanstab = enleverDiez(intermediaire[1]); 
		if(idDanstab.equals(id)) {
		    String rep = intermediaire[0] + " " + intermediaire[2] + " " + intermediaire[3] + " "+ intermediaire[4] +" " +  intermediaire[5] ;
		    return rep ; 
		}
	    }
	}
	return "" ; 

    }

    public static void main(String[] Args){
	System.out.println("Client Main");
	ecouteDiffuseur chaine = null; 
	ecrireAuDiffuseur action = null ;
	Thread t = null , t1 = null; 
	String [] info  = null; 
	String [] ListeDiffuseur = null ; 
	System.out.println ("Choississez votre pseudo :") ;
	BufferedReader br = new BufferedReader (new InputStreamReader (System.in ));
	String id = "Default" ; 
	boolean begin = true ;
	    try {
		id = br.readLine() ; 
	    }
	    catch (Exception  e) {
		System.out.println(e) ; 
	    }
       	System.out.println("Seul 5 commande sont accepté : LIST , RADIO IPV4 port IPV4 port2  , MESS message, LAST num , DECO , QUIT  ") ; 
	while (begin) {
	    try {
	    String c = br.readLine() ;
	    String [] commande = c.split(" ") ; 
	     System.out.println("commande retenue -> " + c + " | taille commande -> " + commande.length) ; 
	     /*   System.out.println(commande[0] + " " +"RADIO" + " -> " + commande[0].equals("RADIO")) ;  */
	    if ((commande[0].equals("LIST"))&&(commande.length == 1 ) )  {
		ListeDiffuseur = afficheDiffuseur() ; 
		if(info == null) {
		    System.out.println("Voulez vous vous connectez à un diffuseur?(y\n)") ;
		    String rep = br.readLine() ;
		    if (rep.equals("y")){
			System.out.println("Veuillez introduire l id du diffuseur") ;
			String idDiffus = br.readLine() ;
			String information = verif(ListeDiffuseur , idDiffus) ; 
			if (!information.equals("")){
			    info = information.split(" ") ; 
			    chaine = new ecouteDiffuseur(information) ; 
			    t = new Thread(chaine) ;
			    t.start() ; 
			}
			else {
			    System.out.println("id invalide") ; 
			}
			
		    }
		}
	    }
	    else if ((commande[0].equals("DECO"))&&(commande.length == 1 ) ) {
		if((chaine != null )&&(t != null)) {
			chaine.Allumer = false ;
			System.out.println("Veuillez attendre 3 seconde ") ; 
			Thread.sleep(3000) ; 
			if (t.getState()!=Thread.State.TERMINATED){
			    System.out.println("Déconnexion accomplie" ) ; 
			}
			else {
			    System.out.println("erreur de fonctionement , vérifier que le diffuseur est toujours actif avant de recommencez") ; 
			}
		    }
		    else {
			System.out.println("Vous n'êtes connectez à aucun diffuseur " ) ; 
		    }
		}
	    else if ((commande.length ==5)&&(commande[0].equals("RADIO")) ) {
		if (info == null) {
		    info = c.split(" ") ; 
		    chaine = new ecouteDiffuseur(c) ;
		    t =  new Thread(chaine ) ;
		    t.start() ;  
		    Thread.sleep(2000) ; 
		    if(t.getState()==Thread.State.TERMINATED) {
			info = null ;
			chaine = null ; 
			System.out.println("erreur de connexion") ; 
		    }
		}
		else {
		    System.out.println("Vous devez vous deconnectez avant de pouvoir écouter un autre diffuseur") ; 
		}
	    }
	    else if ((commande[0].equals("QUIT"))&&(commande.length == 1 ) )  {
		if ((chaine != null )&&(t!= null)) {
		    chaine.Allumer = false ; 
		    Thread.sleep(3000) ; 
		    if(t.getState()!=Thread.State.TERMINATED) {
			System.out.println("Déconnexion accomplie");
		    }
		}
		begin = false ; 
	    }
	    
	    else {
		if(info != null) {
		    action = new ecrireAuDiffuseur(info[3] ,info[4] , c , id) ;
		    t1 = new Thread(action) ; 
		    t1.start() ;
		    
		}
		else {
		    System.out.println("Vous n'êtes pas connecter a un diffuseur") ; 
		}
	    
	    }
	    }
	    catch (Exception e) {
		begin = false ; 
		System.out.println("erreur br") ; 
	    }
	}
	System.out.println("Au revoir" ) ;
    }
}
