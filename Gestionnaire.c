//
//  test.c
//
//
//  Created by Wintermute on 29/04/2015.
//
//


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>

#define SIZEMESSAGE 126
#define	MAXDIFF		10
#define PORT "9034"   // port we're listening on

static const double TIME_PING = (double) (5);
static const double TIME_OUT = (double) (10);

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


struct Liste_diff {
  char info [128] ; 
  struct Liste_diff * suivant ; 

};
typedef struct Liste_diff liste_diff ; 

liste_diff*  copierListe (int * nbDiff,int  diff[3][MAXDIFF], char  listDiff[MAXDIFF][128] ) { 
  liste_diff *pointeur = calloc(1,sizeof(liste_diff));
  liste_diff *lsec = pointeur ; 
  int compte = 0 ;
  int i ; 
  for (i = 0 ; i <MAXDIFF ; i ++) {
    if (diff[1][i] != -1) {
      int indice = diff[1][i] ; 
    strcpy(pointeur->info,listDiff[indice]); 
    compte ++ ;
    pointeur->suivant =  calloc(1,sizeof(liste_diff));; 
    pointeur = pointeur->suivant; 
    }
  }
  *(nbDiff) = compte ; 
  
  return lsec ; 
}

char *affichage (liste_diff *encour ) {
  char *la = calloc(128,sizeof(char) ) ;
  strcat(la,"ITEM ") ; 
  strcat(la,(encour->info)) ;
  strcat(la,"\r\n");
  return la;
}
int envoyezLaListe (int socket,int diff[3][MAXDIFF] , char  ListDiff[MAXDIFF][128]) {
  int n =0; 
  liste_diff * c  = copierListe (&n, diff ,ListDiff) ;
  char nbrDiff [12] = "LINB ";
  char nEnChar [3] ; 
  memset(nEnChar,'\0',sizeof(char)*3);
  sprintf(nEnChar ,"%d",n);
  strcat(nbrDiff ,nEnChar );
  strcat(nbrDiff, "\r\n");
  if (send(socket, nbrDiff, strlen(nbrDiff), 0) == -1) {
    perror("send !!!");
  }
  while(c!= NULL && n >0 ) {
    char * diffuseur = affichage(c) ;
    write(socket,diffuseur , strlen(diffuseur));
    c = c->suivant ;
    n -- ; 
    free(diffuseur) ; 
  }
  return 0 ;
}
int main(void){
    
    time_t timetemp;
    time_t timestamp[MAXDIFF];
    double difference;
    int diff[3][MAXDIFF];
    char listDiff[MAXDIFF][128];
    char *mess;
    
    fd_set master;    // master file descriptor list
    fd_set read_fds;  // temp file descriptor list for select()
    int fdmax;        // maximum file descriptor number
    
    int listener;     // listening socket descriptor
    int newfd;        // newly accept()ed socket descriptor
    struct sockaddr_storage remoteaddr; // client address
    socklen_t addrlen;
    
    char buff[256];    // buffer for client data
    int nbytes;
    
    //gestion du timeout (mis à 1s)
    struct timeval tv;
    tv.tv_sec = 5;
    tv.tv_usec = 0;
    
    char remoteIP[INET6_ADDRSTRLEN];
    
    int yes=1;        // for setsockopt() SO_REUSEADDR, below
    int i, j, rv,k,temp,l;
    int a = 0;
    struct addrinfo hints, *ai, *p;
    for (i=0; i<MAXDIFF; i++) {
        memset(listDiff[i], '\0', sizeof(char)*128);
    }
    memset(diff[0],-1, sizeof(int)*MAXDIFF);
    memset(diff[1],-1, sizeof(int)*MAXDIFF);
    memset(diff[2], 0, sizeof(int)*MAXDIFF);
    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);
    
    
    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((rv = getaddrinfo(NULL, PORT, &hints, &ai)) != 0) {
        fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
        //exit(1);
    }
    
    for(p = ai; p != NULL; p = p->ai_next) {
        listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listener < 0) {
            continue;
        }
        
        // lose the pesky "address already in use" error message
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
        
        if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
            close(listener);
            continue;
        }
        
        break;
    }
    
    // if we got here, it means we didn't get bound
    if (p == NULL) {
        fprintf(stderr, "selectserver: failed to bind\n");
        //exit(2);
    }
    
    freeaddrinfo(ai); // all done with this
    
    // listen
    if (listen(listener, 10) == -1) {
        perror("listen");
        //exit(3);
    }
    
    // add the listener to the master set
    FD_SET(listener, &master);
    
    // keep track of the biggest file descriptor
    fdmax = listener; // so far, it's this one
    
    // main loop
    for(;;) {
        read_fds = master; // copy it
        if (select(fdmax+1, &read_fds, NULL, NULL, &tv) == -1) {
            perror("select");
            //exit(4);
        }
        
        // run through the existing connections looking for data to read
        for(i = 0; i <= fdmax; i++) {
        
            if (FD_ISSET(i, &read_fds)) { // we got one!!
                if (i == listener) {
                    // handle new connections
                    addrlen = sizeof remoteaddr;
                    newfd = accept(listener,
                                   (struct sockaddr *)&remoteaddr,
                                   &addrlen);
                    
                    if (newfd == -1) {
                        perror("accept");
                    } else {
                        FD_SET(newfd, &master); // add to master set
                        if (newfd > fdmax) {    // keep track of the max
                            fdmax = newfd;
                        }
                        printf("selectserver: new connection from %s on "
                               "socket %d\n",
                               inet_ntop(remoteaddr.ss_family,
                                         get_in_addr((struct sockaddr*)&remoteaddr),
                                         remoteIP, INET6_ADDRSTRLEN),
                               newfd);
                    }
                }
                else {
                    
                    // handle data from a client
                    nbytes = recv(i, buff, sizeof buff, 0);
                    
                    if (nbytes <=0) {
                        // got error or connection closed by client
                        // connection closed
                        //printf("nbytes: %i poof plus de %i\n",nbytes,i);
                        temp=-1;
                        //retire du tableau des serv si c'était un serv
                        for (j=0; j<MAXDIFF; j++) {
                            if (diff[0][j]==i) {
                                temp=diff[0][j];
                                temp=diff[1][j];
                                diff[2][j]=0;
                                //break;
                            }
                        }
                        if (temp!=-1){
                            memset(listDiff[temp], '\0', sizeof(char)*16);
                        }
                        
                        close(i); // bye!
                        FD_CLR(i, &master); // remove from master set
                        
                    }
                    else {
                        // we got some data from a client
                        printf("\nnbytes: %i n",nbytes);
                        printf("socket %i: %s\n",i,buff);

			//----------------------------------------                        Liste 
                        if (strncmp(buff,"LIST",4)==0) {
			  envoyezLaListe(i,diff , listDiff);
			  
                            //exit(0);
                        }
                        
                        else if(strncmp(buff,"IMOK",4)==0){
                            for (j=0; j<MAXDIFF; j++) {
                                if(diff[0][j]==i){
                                    diff[2][j]=0;
                                    timestamp[j]=time(NULL);
                                }
                            }
                            
			}//TODO remettre les bonnes valeurs
                        else if (nbytes== SIZEMESSAGE+2|| nbytes> 30) {// gestion de la co d'un nouveau servs
                            if (strncmp(buff,"REGI ",5)==0){
                                printf("\n          ---------------------------------------\n           demande d'enregistrement sur la socket %i\n\n",i);
                                if (a<MAXDIFF) {
                                    printf("        assez de place\n");
                                    
                                    
                                    
                                    for (k=0; k<MAXDIFF; k++) {
                                        if (diff[0][k]==-1) {
                                            
                                            memcpy(listDiff[k],buff+5,128);
                                            printf("            ajout de: %s\n" ,listDiff[k]);
                                            printf("                dans listDiff[%i]\n",k);

                                            
                                            diff[0][k]=i;
                                            diff[1][k]=k;
                                            diff[2][k]=0;
                                            timestamp[k]=time(NULL);
                                            printf("                ajout en %i fini: %i %i %i \n",k,diff[0][k],diff[1][k],diff[2][k]);
                                            break;
                                            
                                        }
                                    }
                                    a++;
                                    mess="REOK\r\n";
                                    printf("                    REOK\n          ---------------------------------------\n\n");
                                    if (send(i, mess, 6, 0) == -1) {
				      perror("send !!!");
				    }
				    //fflush(i);
                                }
                                else {
                                    mess="RENO\n";
                                    printf("                    RENO\n");
                                    if (send(i, mess, 6, 0) == -1) {
                                        perror("send");
                                    }
				    //fflush(i);
                                    close(i); // bye!
                                    FD_CLR(i, &master);
                                }
                            }
                            else{
                                printf("pas de REGI ");
                            }
                        }
                        else{
                            
                        }
                        
                        memset(buff, '\0', 256);
                        // run through the existing connections looking for timestamp
                        
                        
                    }//END handle else
                    
                } // END handle data from client
            } // END got new incoming connection
        } // END looping through file descriptors
        
        timetemp=time(NULL);
        for(l = 0; l < MAXDIFF; l++) {
            if (diff[0][l]!=-1) {//we've got a serv
               // printf("%i\n",l);
                if (diff[2][l]==0) {//on dois regarder si on ping
		  //printf("    RUOK test for nsocket %i at ",diff[0][l]);
                    difference=difftime(timetemp,timestamp[l]);
		    //printf("%f s\n" ,difference);
                    if (difference>=TIME_PING) {
                        mess="RUOK\r\n";
			printf("MESS : %s \n", mess);
                        if (send(diff[0][l], mess, 6, 0) == -1) {
                            perror("send");
                        }
			//fflush(diff[0][l]);
                        diff[2][l]=1;
                        timestamp[l]=time(NULL);
                    }
                }
                else if(diff[2][l]==1){//on doit regarder si il a timeout
		  //printf("    TimeOut test for nsocket %i at ",diff[0][l]);
                    difference=difftime(timetemp,timestamp[l]);
		    //printf("%f s\n" ,difference);
                    if ((difference=difftime(timetemp,timestamp[l]))>=TIME_OUT) {
                        printf("\n          ---------------------------------------\n           serveur %i on socket %i timed out\n",l,diff[0][l]);
                        
                        diff[1][l]=-1;
                        diff[2][l]=0;
                        close(diff[0][l]); // bye!
                        FD_CLR(diff[0][l], &master);
                        diff[0][l]=-1;
                        printf("%i %i %i\n          ---------------------------------------\n",diff[0][l],diff[1][l],diff[2][l]);
                    }
                }
                else{// TODO not normal, we'll ping and put a normal value in diff[2]
                    printf("diff of %i: %i %i %i\n time of %i: %ld\n\n ",i,diff[0][l],diff[1][l],diff[2][l],l,timestamp[l]);
                }
            }
        }
    } // END for(;;)--and you thought it would never end!
    return 0;
}

